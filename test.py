import numpy as np
from sklearn.datasets import load_diabetes
from sklearn.metrics import r2_score
from clearml import Task, Dataset,Logger
import joblib

task = Task.init(project_name="Regression Example", task_name="Diabetes Regression")

# Load the diabetes dataset
diabetes = load_diabetes()

# Load the trained model from ClearML
#model_artifact = task.artifacts['model.pkl']
model = joblib.load('model.pkl')

# Evaluate the model on the entire dataset
X = diabetes.data
y = diabetes.target
y_pred = model.predict(X)

# Calculate the R^2 score and log it to ClearML
score = r2_score(y, y_pred)
print(score)
logger = Logger.current_logger()
logger.report_single_value("test_score",score)

