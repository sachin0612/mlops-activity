import joblib
from flask import Flask, jsonify, request

# Load the trained model
model = joblib.load('model.pkl')

# Create a Flask app
app = Flask(__name__)

# Define a route for making predictions
@app.route('/predict', methods=['POST'])
def predict():
    # Get the request data
    data = request.json

    # Prepare the input data for prediction
    X = [[data['age'], data['sex'], data['bmi'], data['bp'], data['s1'], data['s2'], data['s3'], data['s4'], data['s5'], data['s6']]]

    # Make a prediction on the input data
    prediction = model.predict(X)

    # Create a JSON response with the prediction
    response = {'prediction': int(prediction[0])}

    # Return the response
    return jsonify(response)

# Run the app
if __name__ == '__main__':
    app.run()
