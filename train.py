import numpy as np
import pandas as pd
from sklearn.datasets import load_diabetes
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split
from clearml import Task, Dataset,Logger
import joblib

# Initialize the ClearML task and dataset
task = Task.init(project_name="Regression Example", task_name="Diabetes Regression")
dataset = Dataset.create(dataset_name="diabetes_dataset", dataset_project='Diabetes Prediction')
dataset.add_files('diabetes.tab.txt')


# Load the Diabetes dataset
diabetes = load_diabetes()

# Convert the dataset to a Pandas dataframe
data = pd.DataFrame(diabetes.data, columns=diabetes.feature_names)
data["target"] = diabetes.target

# Preprocess the data
X = data.drop("target", axis=1)
y = data["target"]
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Log the dataset
dataset.upload()

# Train the model
model = LinearRegression()
model.fit(X_train, y_train)

# Log the model
task.upload_artifact("model.pkl", model)
joblib.dump(model,'model.pkl')
# Evaluate the model
train_score = model.score(X_train, y_train)
test_score = model.score(X_test, y_test)
logger = Logger.current_logger()
# Log the scores
logger.report_single_value("train_score", train_score)
logger.report_single_value("test_score", test_score)
